$('.search__btn').click(function(e) {
  e.preventDefault();
  $('.search').toggleClass('search--active');
  $('.search__input').focus();
  $('body').removeClass('body--fixed');
});
