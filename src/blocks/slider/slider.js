$(window).on('load', function () {
  $('.slider').mCustomScrollbar({
    axis: "x",
    scrollInertia: false,
    scrollInertia: 500,
    scrollButtons: {
      enable: true,
      scrollAmount: 50
    }
  });

  setTimeout(function () {
    $('.mCSB_container').css('left', 0);
    $('.mCSB_dragger').css('left', 0);
  }, 500)

})




$(window).on('load', function () {
  var arrowWrapperWidth = ($(window).width() - $('.t604__imgwrapper').width()) / 2;
  $('.t-slds__arrow_wrapper').css('width', arrowWrapperWidth);
});
