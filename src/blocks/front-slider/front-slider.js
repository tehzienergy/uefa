var frontSlider = $('.front-slider');

frontSlider.slick({
  infinite: true,
  dots: true,
  arrows: false,
  autoplay: true,
  fade: true,
  autoplaySpeed: 5000,
  responsive: [
    {
      breakpoint: 993,
//      settings: 'unslick',
      settings: {
        variableWidth: true,
        dots: false,
        fade: false
      }
    },
  ],
});
