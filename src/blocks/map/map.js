(() => {
  let maps = document.getElementsByClassName('map');
  let container = document.querySelector('.map');
  for (let c = 0; c < maps.length; c++) {
    maps[c].id = 'map-' + c;
    ymaps.ready(init);

    function init() {
      let myMap = new ymaps.Map('map-' + c, {
        center: [59.685739, 30.452675],
        controls: [],
        zoom: 16,
      }, {
          searchControlProvider: 'yandex#search'
        }),

      myPlacemark = new ymaps.Placemark([59.685739, 30.452675], {
        hintContent: '',
        balloonContent: ''
      }, {
          iconLayout: 'default#image',
          iconImageHref: 'img/placemark.svg',
          iconImageSize: [82, 87],
          iconImageOffset: [-41, -87]
        })

      myMap.geoObjects
        .add(myPlacemark);

      myMap.behaviors.disable('scrollZoom');
      myMap.behaviors.disable('multiTouch');
    }
  };
})();
