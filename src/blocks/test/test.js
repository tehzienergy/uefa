$('.js-test-start').click(function(e) {
  e.preventDefault();
  $('.test__start').hide();
  $('.test__body-wrapper').fadeIn('fast');
});

/* Тестовый скрипт для наглядности смены состояний геотеста */
$('.js-test-final').click(function(e) {
  e.preventDefault();
  $('.test__content').hide();
  $('.test__final').fadeIn('fast');
  $('.test__progress').hide();
});

$('.test__answer-close').click(function(e) {
  e.preventDefault();
  $(this).closest('.test__answer').fadeOut();
  $('.test__item--active').removeClass('test__item--answered');
});

//$('.test__close').click(function(e) {
//  e.preventDefault();
//  $('.test').fadeOut();
//})



if ($(window).width() < 1200) {
  
  // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
  let vh = window.innerHeight * 0.01;
  // Then we set the value in the --vh custom property to the root of the document
  document.documentElement.style.setProperty('--vh', `${vh}px`);


  window.addEventListener('resize', () => {
    // We execute the same script as before
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
  });
}

